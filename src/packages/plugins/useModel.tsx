import { ref, watch, defineComponent } from 'vue';

export function useModel<T>(getter: () => T, emitter: (val: T) => void) {
    // 获取getter函数的返回值，并单独保存一份
    const state = ref(getter()) as { value: T };
    // 观察getter，取值操作，则重新给state赋值， 测试时，两个TestUserModel需要这个
    watch(getter, (val: T) => {
        if (val !== state.value) {
            state.value = val;
        }
    })
    // 返回计算属性，获取
    return {
        get value() { return state.value },
        set value(val: T) {
            if (state.value !== val) {
                state.value = val;
                emitter(val);
            }
        }
    }
}

// 测试useModel
export const TestUseModel = defineComponent({
    props: {
        modelValue: {
            type: Object
        }
    },
    emits: {
        'update:modelValue': (val?: any) => {
            return true;
        }
    },
    setup(props, ctx) {
        const model: any = useModel(() => props.modelValue, val => ctx.emit('update:modelValue', val));
        // console.log('model :>> ', model);
        setTimeout(() => {
            model.value.blocks = [{
                componentKey: "text",
                top: 100,
                left: 100
            },
            {
                componentKey: "input",
                top: 300,
                left: 300
            }]
            model.value.container.width = 50;
        }, 3000)
        return () => (
            <div>
                自定义的输入框
                {
                    (model.value.blocks || []).map((item: any) => (
                        <div>{item.componentKey}</div>
                    ))
                }
                <div>containerWidth: {model.value.container.width}</div>
            </div>
        )
    }
})