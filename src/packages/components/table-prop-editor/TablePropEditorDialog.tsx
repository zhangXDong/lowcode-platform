import { defineComponent, PropType, getCurrentInstance, reactive, onMounted, createApp } from 'vue';
// 组件
import { ElButton, ElInput, ElDialog, ElTable, ElTableColumn } from 'element-plus';
// 类型声明
import { TablePropEditorOption } from './Index.d';
// 插件
import { defer } from '../../utils/defer';
import deepcopy from 'deepcopy';

// 定义组件
const ServiceComponent = defineComponent({
    props: {
        option: {
            type: Object as PropType<TablePropEditorOption>,
            required: true
        }
    },
    setup(props) {
        // 获取当前组件实例，用于将将service绑定到实例上
        const ctx = getCurrentInstance()!;

        // 响应式数据
        const state = reactive({
            option: props.option,
            showFlag: false, // 展示弹窗
            mounted: (() => {
                const dfd = defer();
                onMounted(() => setTimeout(() => dfd.resolve(), 0))
                return dfd.promise;
            })(),
            editData: [] as any[] // 当前编辑的数据
        })

        // 方法
        const methods = {
            service: (option: TablePropEditorOption) => {
                state.option = option;
                state.editData = deepcopy(option.data || []);
                methods.show();
            },
            show: async () => {
                await state.mounted;
                state.showFlag = true;
            },
            hide: () => {
                state.showFlag = false;
            },
            add: () => {
                state.editData.push({});
            },
            reset: () => {
                state.editData = deepcopy(state.option.data);
            }
        }

        const handler = {
            onCancel: () => {
                methods.hide();
            },
            onConfirm: () => {
                state.option.onConfirm(state.editData);
                methods.hide();
            },
            onDelete: (index: number) => {
                state.editData.splice(index, 1);
            }
        }


        Object.assign(ctx.proxy, methods);

        // @ts-ignore
        return () => <ElDialog v-model={state.showFlag}>
            {{
                default: () => (
                    <div>
                        <div>
                            <ElButton type="primary" {...{ onClick: methods.add } as any}>添加</ElButton>
                            <ElButton {...{ onClick: methods.reset } as any}>重置</ElButton>
                        </div>

                        <ElTable data={state.editData}>
                            <ElTableColumn {...{ type: 'index' } as any} />
                            {state.option.config.table!.options.map((item: any) => (
                                <ElTableColumn {...{ label: item.label } as any}>
                                    {{
                                        default: ({ row }: { row: any }) => {
                                            return <ElInput v-model={row[item.field]} />
                                        }
                                    }}
                                </ElTableColumn>
                            ))}
                            <ElTableColumn {...{ label: '操作栏' } as any}>
                                {{
                                    default: ({ $index }: { $index: number }) => <ElButton
                                        type="danger"
                                        {...{ onClick: () => handler.onDelete($index) } as any}
                                        >删除
                                    </ElButton>

                                }}
                            </ElTableColumn>
                        </ElTable>
                    </div>
                ),
                footer: () => <>
                    <ElButton {...{ onClick: handler.onCancel } as any}>取消</ElButton>
                    <ElButton type="primary" {...{ onClick: handler.onConfirm } as any}>确定</ElButton>
                </>
            }}
        </ElDialog>
    }
})

const $$TablePropEditorDialog = (() => {
    let instance: any;
    return (option: Omit<TablePropEditorOption, 'onConfirm'>) => {
        if (!instance) {
            const el = document.createElement('div');
            document.body.appendChild(el);
            const app = createApp(ServiceComponent, { option });
            instance = app.mount(el);
        }
        const dfd = defer<any[]>();
        instance.service({
            ...option,
            onConfirm: dfd.resolve
        });
        return dfd.promise;
    }
})()

export default $$TablePropEditorDialog;
