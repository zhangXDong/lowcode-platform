import { VisualEditorComponent } from '../../layout/visual-editor-menu/Index.d';
import { VisualEditorBlockData } from './Index.d';

// 创建block
export function createNewBlock({ component, top, left }: { component: VisualEditorComponent, top: number, left: number }): VisualEditorBlockData {
    return {
        componentKey: component!.key, // input、text...
        width: 0,
        height: 0,
        top,  // 距离顶部位置
        left, // 距离左侧位置
        adjustPosition: true, // 鼠标松开是否调整block位置居中
        focus: false, // 是否为选中态
        zIndex: 0, // 权重，用于置顶置底
        hasResize: false, // 是否已经改变了block的宽高，改变了则使用数据中保存的宽高
        props: {}, // 配置的属性
        model: {}  // 绑定的字段
    }
}